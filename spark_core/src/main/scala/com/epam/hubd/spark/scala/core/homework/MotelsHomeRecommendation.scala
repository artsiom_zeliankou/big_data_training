package com.epam.hubd.spark.scala.core.homework

import com.epam.hubd.spark.scala.core.homework.domain.{BidError, BidItem, EnrichedItem, EnrichmentKey}
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.mutable.ListBuffer
import scala.util.control.Exception._

object MotelsHomeRecommendation {

  val ERRONEOUS_DIR: String = "erroneous"
  val AGGREGATED_DIR: String = "aggregated"

  def main(args: Array[String]): Unit = {
    require(args.length == 4, "Provide parameters in this order: bidsPath, motelsPath, exchangeRatesPath, outputBasePath")

    val bidsPath = args(0)
    val motelsPath = args(1)
    val exchangeRatesPath = args(2)
    val outputBasePath = args(3)

    val sc = new SparkContext(new SparkConf().setAppName("motels-home-recommendation"))

    processData(sc, bidsPath, motelsPath, exchangeRatesPath, outputBasePath)

    sc.stop()
  }

  def processData(sc: SparkContext, bidsPath: String, motelsPath: String, exchangeRatesPath: String, outputBasePath: String) = {

    /**
      * Task 1:
      * Read the bid data from the provided file.
      */
    val rawBids: RDD[List[String]] = getRawBids(sc, bidsPath)
    /**
      * Task 1:
      * Collect the errors and save the result.
      * Hint: Use the BideError case class
      */
    val erroneousRecords: RDD[String] = getErroneousRecords(rawBids)
    erroneousRecords.saveAsTextFile(s"$outputBasePath/$ERRONEOUS_DIR")

    /**
      * Task 2:
      * Read the exchange rate information.
      * Hint: You will need a mapping between a date/time and rate
      */
    val exchangeRates: Map[String, Double] = getExchangeRates(sc, exchangeRatesPath)

    /**
      * Task 3:
      * Transform the rawBids and use the BidItem case class.
      * - Convert USD to EUR. The result should be rounded to 3 decimal precision.
      * - Convert dates to proper format - use formats in Constants util class
      * - Get rid of records where there is no price for a Losa or the price is not a proper decimal number
      */
    val bids: RDD[BidItem] = getBids(rawBids, exchangeRates)

    /**
      * Task 4:
      * Load motels data.
      * Hint: You will need the motels name for enrichment and you will use the id for join
      */
    val motels: RDD[(String, String)] = getMotels(sc, motelsPath)

    /**
      * Task5:
      * Join the bids with motel names and utilize EnrichedItem case class.
      * Hint: When determining the maximum if the same price appears twice then keep the first entity you found
      * with the given price.
      */
    val enriched:RDD[EnrichedItem] = getEnriched(bids, motels)
    enriched.saveAsTextFile(s"$outputBasePath/$AGGREGATED_DIR")
  }

  def getRawBids(sc: SparkContext, bidsPath: String): RDD[List[String]] = {
    sc
        .textFile(bidsPath)
        .map(line => line.split(Constants.DELIMITER) toList)
  }

  def getErroneousRecords(rawBids: RDD[List[String]]): RDD[String] = {
    rawBids
          .filter(_ (2) contains "ERROR_")
          .map(item => (new BidError(item(1).trim, item(2).trim), 1))
          .reduceByKey(_ + _)
          .map(_.productIterator.mkString(Constants.DELIMITER))     // .map(pair => pair._1 + Constants.DELIMITER + pair._2)
  }

  def getExchangeRates(sc: SparkContext, exchangeRatesPath: String): Map[String, Double] = {
    sc
        .textFile(exchangeRatesPath)
        .map(line => line.split(Constants.DELIMITER) toList)
        .map(item => (item(0).trim, item(3).trim.toDouble))
        .collect()
        .toMap
  }

  def getBids(rawBids: RDD[List[String]], exchangeRates: Map[String, Double]): RDD[BidItem] = {
    rawBids
          .filter(lines=> ! lines(2).contains("ERROR_"))
          .map(x=>explodeBid(x, exchangeRates))
          .flatMap(y=>y)
  }

  def explodeBid(item :List[String], exchangeRates: Map[String, Double]) : ListBuffer[BidItem] = {
    val motelId: String = item(0)
    val bidDate: String = convertDateTime(item(1))
    val exchangeRate: Double = exchangeRates(item(1))
    val precision:Int = 3
    var bids = new ListBuffer[BidItem]()

    val usRate: Double = parseToDouble(item(5)).getOrElse(0.0)
    val mxRate: Double = parseToDouble(item(6)).getOrElse(0.0)
    val caRate: Double = parseToDouble(item(8)).getOrElse(0.0)
     if (usRate != 0.0) {
       val price = roundDoubleWithPrecise(usRate * exchangeRate, precision)
       bids += BidItem(motelId, bidDate, Constants.TARGET_LOSAS(0), price)
     }

    if (mxRate != 0.0) {
      val price = roundDoubleWithPrecise(mxRate * exchangeRate, precision)
       bids += BidItem(motelId, bidDate, Constants.TARGET_LOSAS(2), price)
    }

    if (caRate != 0.0) {
      val price = roundDoubleWithPrecise(caRate * exchangeRate, precision)
       bids += BidItem(motelId, bidDate, Constants.TARGET_LOSAS(1), price)
    }

    bids
  }

  def convertDateTime(inputDateString: String): String = {
    Constants.INPUT_DATE_FORMAT.parseDateTime(inputDateString).toString(Constants.OUTPUT_DATE_FORMAT)
  }

  def parseToDouble(price: String): Option[Double] = {
    catching(classOf[NumberFormatException]) opt price.trim.toDouble
  }

  def roundDoubleWithPrecise(origin: Double, precise: Int): Double = {
    val s = math pow (10, precise); (math round origin * s) / s
  }

  def getMotels(sc:SparkContext, motelsPath: String): RDD[(String, String)] = {
    sc
        .textFile(motelsPath)
        .map(line => line.split(Constants.DELIMITER) toList)
        .map(item => (item(0).trim, item(1).trim))
  }

  def findIdItemWithHigherPrice(item1: BidItem, item2: BidItem): BidItem = {
    if (item1.price >= item2.price) item1 else item2
  }

  def getEnriched(bids: RDD[BidItem], motels: RDD[(String, String)]): RDD[EnrichedItem] = {
    bids
        .map( bidItem => ( new EnrichmentKey(bidItem.motelId, bidItem.bidDate ), bidItem) )
        .reduceByKey( (x, y) => findIdItemWithHigherPrice(x,y) )
        .map(pair => (pair._1.motelId, pair._2) )
        .join(motels)
        .map(x => new EnrichedItem(x._1, x._2._2, x._2._1.bidDate, x._2._1.loSa, x._2._1.price))
  }

}
