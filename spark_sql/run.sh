#!/bin/bash
#set variables
inputDir=/input_data/artsiom_zeliankou/spark/sql
outputDir=/output_data/artsiom_zeliankou/spark/sql
bids=${inputDir}/bids
motels=${inputDir}/motels
exchange_rates=${inputDir}/exchange_rates
#
#prepare working directories
hdfs dfs -rm -r $inputDir
hdfs dfs -mkdir -p $inputDir
hdfs dfs -mkdir -p $bids
hdfs dfs -mkdir -p $motels
hdfs dfs -mkdir -p $exchange_rates
hdfs dfs -rm -r $outputDir
#
#copy input files to HDFS
hdfs dfs -copyFromLocal $1 ${bids}/
hdfs dfs -copyFromLocal $2 ${motels}/
hdfs dfs -copyFromLocal $3 ${exchange_rates}/
#
mvn clean install
#
spark-submit --master yarn-client --driver-memory 1g --num-executors 2 --executor-memory 1g --conf spark.executor.cores=2 target/spark-sql.jar $bids $motels $exchange_rates $outputDir