package com.epam.hubd.spark.scala.sql.homework

import org.apache.spark.sql._
import org.apache.spark.sql.functions._
import org.apache.spark.sql.expressions.UserDefinedFunction
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession

import scala.util.control.Exception.catching

object MotelsHomeRecommendation {

  val ERRONEOUS_DIR: String = "erroneous"
  val AGGREGATED_DIR: String = "aggregated"

  def main(args: Array[String]): Unit = {
    require(args.length == 4, "Provide parameters in this order: bidsPath, motelsPath, exchangeRatesPath, outputBasePath")

    val bidsPath = args(0)
    val motelsPath = args(1)
    val exchangeRatesPath = args(2)
    val outputBasePath = args(3)

    val conf = new SparkConf().setAppName("motels-home-recommendation")
    val sparkSession = SparkSession.builder.config(conf).getOrCreate


    processData(sparkSession, bidsPath, motelsPath, exchangeRatesPath, outputBasePath)

    sparkSession.stop()
  }

  def processData(sparkSession: SparkSession, bidsPath: String, motelsPath: String, exchangeRatesPath: String, outputBasePath: String) = {

    /**
      * Task 1:
      * Read the bid data from the provided file.
      */
    val rawBids: DataFrame = getRawBids(sparkSession, bidsPath)

    /**
      * Task 1:
      * Collect the errors and save the result.
      */
    val erroneousRecords: DataFrame = getErroneousRecords(rawBids)
    erroneousRecords.write
      .format(Constants.CSV_FORMAT)
      .save(s"$outputBasePath/$ERRONEOUS_DIR")

    /**
      * Task 2:
      * Read the exchange rate information.
      * Hint: You will need a mapping between a date/time and rate
      */
    val exchangeRates: DataFrame = getExchangeRates(sparkSession, exchangeRatesPath)

    /**
      * Task 3:
      * UserDefinedFunction to convert between date formats.
      * Hint: Check the formats defined in Constants class
      */
    val convertDate: UserDefinedFunction = getConvertDate

    /**
      * Task 3:
      * Transform the rawBids
      * - Convert USD to EUR. The result should be rounded to 3 decimal precision.
      * - Convert dates to proper format - use formats in Constants util class
      * - Get rid of records where there is no price for a Losa or the price is not a proper decimal number
      */
    val bids: DataFrame = getBids(rawBids, exchangeRates)

    /**
      * Task 4:
      * Load motels data.
      * Hint: You will need the motels name for enrichment and you will use the id for join
      */
    val motels: DataFrame = getMotels(sparkSession, motelsPath)

    /**
      * Task5:
      * Join the bids with motel names.
      */
    val enriched: DataFrame = getEnriched(bids, motels)
    enriched.write
      .format(Constants.CSV_FORMAT)
      .save(s"$outputBasePath/$AGGREGATED_DIR")
  }

  def getRawBids(sparkSession: SparkSession, bidsPath: String): DataFrame = {
    sparkSession
      .read
      .parquet(bidsPath)
  }

  def getErroneousRecords(rawBids: DataFrame): DataFrame = {
    rawBids
      .filter(rawBids("HU").contains("RROR_"))
  }

  def getExchangeRates(sparkSession: SparkSession, exchangeRatesPath: String): DataFrame = {
    sparkSession
      .read
      .format(Constants.CSV_FORMAT)
      .option("header", "false")
      .schema(Constants.EXCHANGE_RATES_HEADER)
      .load(exchangeRatesPath)
      .drop("CurrencyName", "CurrencyCode")
  }

  def getConvertDate: UserDefinedFunction = {
    udf((inputDateString: String) => Constants.INPUT_DATE_FORMAT.parseDateTime(inputDateString).toString(Constants.OUTPUT_DATE_FORMAT))
  }

  def convertPrice =
    udf((priceUsd: String, exchRate: String)
    => roundDoubleWithPrecise(parseToDouble(priceUsd) * parseToDouble(exchRate), 3)
    )

  def parseToDouble(price: String): Double = {
    val result = catching(classOf[NumberFormatException]) opt price.trim.toDouble
    result.getOrElse(0.0)
  }

  def roundDoubleWithPrecise(origin: Double, precise: Int): Double = {
    val s = math pow(10, precise);
    (math round origin * s) / s
  }

  def getBids(rawBids: DataFrame, exchangeRates: DataFrame): DataFrame = {

    val lightBids =
      rawBids
        .filter(!rawBids("HU").contains("ERROR_"))
        .drop("HU", "UK", "NL", "AU", "CN", "KR", "BE", "I", "JP", "IN", "HN", "GY", "DE")

    lightBids
      .withColumn("CityId", lit(Constants.TARGET_LOSAS(0)))
      .withColumn("Price", col(Constants.TARGET_LOSAS(0)))
      .union(
        lightBids
          .withColumn("CityId", lit(Constants.TARGET_LOSAS(1)))
          .withColumn("Price", col(Constants.TARGET_LOSAS(1)))
      )
      .union(
        lightBids
          .withColumn("CityId", lit(Constants.TARGET_LOSAS(2)))
          .withColumn("Price", col(Constants.TARGET_LOSAS(2)))
      )
      .drop("US", "CA", "MX")
      .join(exchangeRates, col("BidDate").equalTo(exchangeRates("ValidFrom")))
      .withColumn("Date", getConvertDate(col("BidDate")))
      .withColumn("PriceEuro", convertPrice(col("Price"), exchangeRates("ExchangeRate")))
      .drop("BidDate", "Price", "ValidFrom", "ExchangeRate")
      .filter(col("PriceEuro") > 0)
  }

  def getMotels(sparkSession: SparkSession, motelsPath: String): DataFrame = {
    sparkSession
      .read
      .parquet(motelsPath)
      .drop("Country", "URL", "Comment")
  }

  def getEnriched(bids: DataFrame, motels: DataFrame): DataFrame = {

      bids
        .groupBy(col("Date").as("BidDate"), col("MotelID").as("Motel"))
        .max("PriceEuro")
        .join(bids, col("BidDate") === bids("Date") && col("Motel") === bids("MotelID") && col("max(PriceEuro)") === bids("PriceEuro"), "inner")
        .join(motels, col("Motel") === motels("MotelID"))
        .select("Motel", "MotelName", "Date", "CityId", "PriceEuro")

  }
}
