## Input types

There are three kinds of inputs, you can find samples in src/test/resources/input and real data [here](../dataset):

1. bids: contains the bids for the given hotels for a given time for a given country
2. motels: the list of available motels
3. exchange_rate: exchange rates from USD to EUR


## Tasks
1. Filter out the corrupted records from the input and count how many occurred from the given type
   in the given hour.

2. Read the currencies into  a map to use it to exchange the provided USD value to EUR on any given date/time.

3. Get rid of the erroneous records and keep only the conforming ones which are not prefixed
   with ERROR_ strings and left only three countries: US,CA,MX.
  - Convert USD to EUR. The result should be rounded to 3 decimal precision.
  - Convert dates to proper format
  - Get rid of records where there is no price for a Losa or the price is not a proper decimal number

4. Load motels data and prepare it for joining with bids.

5. Enrich the data and only keep the records which have the maximum prices for a given motelId/bidDate.


## How to run:
1. Clone a repository
    git clone https://bitbucket.org/artsiom_zeliankou/big_data_training

2. Go to task directory
    cd spark_sql

3. Run script with 3 parameters
    sh run.sh bids motels exchange_rates

	where
	bids - name of input file with raw bids data
	motels - name of input file with  motels data
	exchange_rates - name of input file with exchange_rate data

	example: sh run.sh ../../dataset/motels.home/bids.gz.parquet ../../dataset/motels.home/motels.gz.parquet ../../dataset/motels.home/exchange_rate.txt