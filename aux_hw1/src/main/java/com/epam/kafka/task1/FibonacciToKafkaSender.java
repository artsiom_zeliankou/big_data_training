package com.epam.kafka.task1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class FibonacciToKafkaSender {

    private final static String KAFKA_TOPIC_NAME = "fib";

    @Value("${fib.row.size}")
    private int fibRowSize;

    @Autowired
    @Qualifier(value = "kafkaTemplate")
    private KafkaTemplate<String, String> kafkaTemplate;

    public void sendFibNumbers() {
        long sum = 0;
        long [] prevValues = {0, 1};
        long currentValue = 0;
        for(long i = 0; i < fibRowSize; i ++) {
            delay();
            String key =  String.valueOf(i);
            if (i < 2) {
                this.kafkaTemplate.send(KAFKA_TOPIC_NAME, key, String.valueOf(i));
            } else {
                currentValue = prevValues[0] + prevValues[1];
                kafkaTemplate.send(KAFKA_TOPIC_NAME, key, String.valueOf(currentValue));
                prevValues[0] = prevValues[1];
                prevValues[1] = currentValue;
            }
        }
    }

    private void delay() {
        try {
            Thread.sleep(300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
