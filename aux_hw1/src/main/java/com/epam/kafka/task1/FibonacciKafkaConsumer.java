package com.epam.kafka.task1;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class FibonacciKafkaConsumer {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    private final static String KAFKA_TOPIC_NAME = "fib";

    @Value("${fib.sum.size}")
    private long fibSumSize;

    private long counter;
    private long sum;

    @KafkaListener(topics = KAFKA_TOPIC_NAME)
    public void process(ConsumerRecord<String, String> cr) throws Exception {
//        logger.info("received message: " + "[ " + cr.key() + " , " + cr.value() + " ]");
        counter ++;
        sum += Long.valueOf(cr.value());
        if (counter % fibSumSize == 0) {
            System.out.println("Sum of  " + counter + " fibonacci numbers = " + sum);
        }
    }

}
