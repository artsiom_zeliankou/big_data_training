package com.epam.kafka.task1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FibonacciKafkaApplication implements ApplicationRunner {

	@Autowired
	private FibonacciToKafkaSender fibonacciSender;

	public static void main(String... args) throws Exception {
		SpringApplication.run(FibonacciKafkaApplication.class, args);
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		fibonacciSender.sendFibNumbers();
	}
}
