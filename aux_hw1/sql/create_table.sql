drop database if exists export;
create database export;
use export;
drop table if exists weather;
create table weather (stateId varchar(20), date date, tmin int, tmax int, snow int, snwd int, prcp int);