#!/usr/bin/env bash
#
inputDir=/input_data/artsiom_zeliankou/aux/weather
datasetInputDir=dataset/weather-table
datasetOutputDir=dataset/weather-table/out
#
hdfs dfs -rm -r $inputDir
hdfs dfs -mkdir -p $inputDir
#
#Copy data to hdfs
unzip -o $datasetInputDir/*.zip -d $datasetOutputDir
hdfs dfs -copyFromLocal $datasetOutputDir/* $inputDir
rm -rf $datasetOutputDir
#
#Create database and table to export to
mysql -u root < sql/create_table.sql
#
#Export data from hdfs
sqoop export --connect jdbc:mysql://127.0.0.1/export --table weather --username root --password root --export-dir /input_data/artsiom_zeliankou/aux/weather -m 8 --input-fields-terminated-by ',' --lines-terminated-by '\n'
#
#Print out  results
mysql -u root  export < sql/select_result.sql;