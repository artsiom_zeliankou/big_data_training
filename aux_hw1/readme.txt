#Kafka:
    0. Run Zookeeper and Kafka via Ambari UI
    1. Create a topic in Kafka from command line :
        sh create_topic.sh

     1.1.  Wait for message with success result: "Created topic "fib"

    2. Build jar file via running a command:
        mvn clean install
    3. Run java application which sends first n Fibonacci numbers through Kafka and calculates the sum of them and writes it to the standard output for every m-th input
    program should be run with arguments:
        fib.row.size - "n"
        fib.sum.size - "m"
        example:    java -jar target/task1-0.0.1-SNAPSHOT.jar --fib.row.size=100 --fib.sum.size=20

     4. Terminate program after last received value via [Ctrl + C]

#Sqoop:
    - Upload the weather data into your HDP sandbox's HDFS (Use the weather table dataset described [here](../dataset/weather-table/)
    - Use sqoop to export all the data to MySQL (the username/password for MySQL on the VM is root/root).
    - Include a screenshot in your report with the result of the following queries run in MySQL:
    ```
            SELECT count(*) FROM weather
            SELECT * FROM weather ORDER BY stationid, data LIMIT 10;
    ```

#Flume
    - Upload linux_messages_3000lines.txt from [here](../dataset/linux_messages_3000lines.txt)
    - Use the following command to create and gradually grow the input:
    ```shell
    cat linux_messages_3000lines.txt | while read line ; do echo "$line" ; sleep 0.2 ; done > output.txt
    ```
    - Write a Flume configuration to upload the ever growing output.txt file to HDFS.
    - Include a screenshot in your report of the output of following command of your output in HDFS:
    ```shell
        hdfs dfs cat ...