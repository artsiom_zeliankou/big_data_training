### DataSet
- [Flying](http://stat-computing.org/dataexpo/2009/the-data.html ) (year 2007)
- [Airports](http://stat-computing.org/dataexpo/2009/supplemental-data.html) (Airports)
- [Carriers](http://stat-computing.org/dataexpo/2009/supplemental-data.html) (Carriers)


### Tasks:
0. Put dataset on HDFS and explore it through Hive.
1. Count total number of flights per carrier in 2007 (Screanshot#1)
2. The total number of flights served in Jun 2007 by NYC (all airports, use join with Airports)(Screanshot#2)
3. Find five most busy airports in US during Jun 01 - Aug 31. (Screanshot#3)
4. Find the carrier who served the biggest number of flights (Screanshot#3)

### Acceptance criteria
- Hive used

### Expected outputs:
	0. ZIP-ed folder with your scripts (table initialization + queries)
	1. Screenshot #1-#4 with executed queries and results.


How to run:
1. Clone a repository
    git clone https://bitbucket.org/artsiom_zeliankou/big_data_training

2. Go to task directory
    cd hive_hw1

3. Run script with 2 parameters
    sh start.sh {userName} {password}

	where
	- userName - hadoop user name
	- password - hadoop user password

	example: sh start.sh raj_ops raj_ops