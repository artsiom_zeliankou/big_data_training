#!/usr/bin/env bash
#
#set variables
airportsDataFile=data/airports.zip
carriersDataFile=data/carriers.zip
flightsDataFile=data/flying.zip
inputDir=/input_data/artsiom_zeliankou/hive/task1
outputDir=/output_data/artsiom_zeliankou/hive/task1
airportsDir=$inputDir/airports
carriersDir=$inputDir/carriers
flightsDir=$inputDir/flights 
HIVE_SCRIPT='run.sh'
#
#setup directories
hdfs dfs -rm -r $inputDir
hdfs dfs -rm -r $outputDir
hdfs dfs -mkdir -p $airportsDir $carriersDir $flightsDir
#
#copy dataset to HDFS
unzip -o ${airportsDataFile}
hdfs dfs -copyFromLocal airports.csv ${airportsDir}/
rm airports.csv
unzip -o ${carriersDataFile}
hdfs dfs -copyFromLocal carriers.csv ${carriersDir}/
rm carriers.csv
unzip -o ${flightsDataFile}
hdfs dfs -copyFromLocal flying.csv ${flightsDir}/
rm flying.csv
#
sh $HIVE_SCRIPT $airportsDir $carriersDir $flightsDir $1 $2