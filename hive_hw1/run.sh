#!/usr/bin/env bash
#
userName=$4
password=$5
#
echo "create external tables from input files"
hive -d airports=$1 -d carriers=$2 -d flights=$3 -f sql/create.sql
#
#explore data
echo "Some records from airports table:"
hive -e "select * from airports limit 5;"
#
echo "Some records from carriers table:"
hive -e "select * from carriers limit 5;"
#
echo "Some records from flights table:"
hive -e "select * from flights limit 5;"
#
echo "Count total number of flights per carrier in 2007"
beeline -u jdbc:hive2:// -n ${userName} -p ${password} -f sql/task1.sql
#
echo "The total number of flights served in Jun 2007 by all New York airports"
beeline -u jdbc:hive2:// -n ${userName} -p ${password} -f sql/task2.sql
#
echo "Find five most busy airports in US during Jun 01 - Aug 31"
beeline -u jdbc:hive2:// -n ${userName} -p ${password} -f sql/task3.sql
#
echo "Find the carrier who served the biggest number of flight"
beeline -u jdbc:hive2:// -n ${userName} -p ${password} -f sql/task4.sql