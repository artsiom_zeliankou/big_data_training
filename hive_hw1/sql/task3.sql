--  Task3. Find five most busy airports in US during Jun 01 - Aug 31
-- A correct way to filter flight date is: to_date(CONCAT_WS('-', flights.year, flights.month, flights.day_of_month)
SELECT air_port, COUNT(flights_number) AS flights_count
FROM(
    SELECT airports.airport AS air_port, flights.flight_num AS flights_number
    FROM flights JOIN airports ON flights.origin=airports.iata
    WHERE airports.country='USA' AND flights.month IN ('6','7','8')
    UNION
    SELECT airports.airport AS air_port, flights.flight_num AS flights_number
    FROM flights JOIN airports ON flights.dest=airports.iata
    WHERE airports.country='USA' AND flights.month IN ('6','7','8')
    ) query
GROUP BY air_port 
ORDER BY flights_count DESC
LIMIT 5;