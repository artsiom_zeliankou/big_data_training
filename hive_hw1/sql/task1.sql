-- Task1. Count total number of flights per carrier in 2007
SELECT carrier_code, count (year)
FROM flights 
WHERE year=2007
GROUP BY carrier_code;