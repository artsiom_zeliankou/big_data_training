--  Task4. Find the carrier who served the biggest number of flights
SELECT carriers.description AS carrier_name, count (flights.year) AS flights_amount
FROM flights JOIN carriers ON flights.carrier_code=carriers.code
GROUP BY carriers.description ORDER BY flights_amount DESC LIMIT 1;
