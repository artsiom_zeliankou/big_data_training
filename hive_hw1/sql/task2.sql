--  Task2. Find the total number of flights served in Jun 2007 by NYC
SELECT COUNT(*) 
FROM(
    SELECT flights.origin, flights.dest, flights.day_of_month, airports.iata, airports.city
    FROM flights
    JOIN airports
    ON flights.origin=airports.iata
    WHERE flights.day_of_month='6' AND airports.city='New York'
    UNION
    SELECT flights.origin, flights.dest, flights.day_of_month, airports.iata, airports.city
    FROM flights
    JOIN airports
    ON flights.dest=airports.iata
    WHERE flights.day_of_month='6' AND airports.city='New York'
) flights;