--  AIRPORTS
-- create external airports table with text output
DROP TABLE IF EXISTS airports_txt;
CREATE EXTERNAL TABLE IF NOT EXISTS airports_txt(
  iata VARCHAR(4),
  airport VARCHAR(40),
  city VARCHAR(20),
  state VARCHAR(20),
  country VARCHAR(20))
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
   'separatorChar' = ',',
   'quoteChar'     = '\"'
) 
STORED AS TEXTFILE
LOCATION "${airports}"
TBLPROPERTIES ("skip.header.line.count"="1");

-- create external airports table with ORC output
DROP TABLE IF EXISTS airports;
CREATE TABLE IF NOT EXISTS airports(
  iata VARCHAR(4),
  airport VARCHAR(40),
  city VARCHAR(20),
  state VARCHAR(20),
  country VARCHAR(20))
ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
STORED AS RCFILE;

-- fill ORC airports table from text table
INSERT OVERWRITE TABLE airports SELECT * FROM airports_txt;

-- drop airports table with text output
DROP TABLE IF EXISTS airports_txt;



--  CARRIERS
-- create external carriers table with text output
DROP TABLE IF EXISTS carriers_txt;
CREATE EXTERNAL TABLE IF NOT EXISTS carriers_txt(
  code VARCHAR(3), description VARCHAR(60))
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
   'separatorChar' = ',',
   'quoteChar'     = '\"'
)
STORED AS TEXTFILE
LOCATION "${carriers}"
TBLPROPERTIES ("skip.header.line.count"="1");

-- create external carriers table with ORC output
DROP TABLE IF EXISTS carriers;
CREATE TABLE IF NOT EXISTS carriers(
  code VARCHAR(3), description VARCHAR(60))
ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
STORED AS RCFILE;

-- fill ORC carriers table from text table
INSERT OVERWRITE TABLE carriers SELECT * FROM carriers_txt;

-- drop carriers table with text output
DROP TABLE IF EXISTS carriers_txt;



--  FLIGHTS
-- create external flights table with text output
DROP TABLE IF EXISTS flights_txt;
CREATE EXTERNAL TABLE IF NOT EXISTS flights_txt(
  year VARCHAR(4), month VARCHAR(2), day_of_month VARCHAR(2), day_of_week SMALLINT,
  dep_time SMALLINT, crs_dep_time SMALLINT,
  arr_time SMALLINT, crs_arr_time SMALLINT,
  carrier_code VARCHAR(3), flight_num SMALLINT, tail_num VARCHAR(5),
  actual_elapsed_time SMALLINT, crs_elapsed_time SMALLINT,
  air_time SMALLINT, arr_delay SMALLINT, dep_delay SMALLINT,
  origin VARCHAR(3), dest VARCHAR(3), 
  distance SMALLINT, taxi_in SMALLINT, taxi_out SMALLINT, 
  cancelled SMALLINT, cancellation_code SMALLINT,
  diverted SMALLINT, carrier_delay SMALLINT, weather_delay SMALLINT, 
  nas_delay SMALLINT, security_delay SMALLINT, late_aircraft_delay SMALLINT)
ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
STORED AS TEXTFILE
LOCATION "${flights}"
TBLPROPERTIES ("skip.header.line.count"="1");


-- create external flights table with ORC output
DROP TABLE IF EXISTS flights;
CREATE TABLE IF NOT EXISTS flights(
  year VARCHAR(4), month VARCHAR(2), day_of_month VARCHAR(2), day_of_week SMALLINT,
  dep_time SMALLINT, crs_dep_time SMALLINT,
  arr_time SMALLINT, crs_arr_time SMALLINT,
  carrier_code VARCHAR(3), flight_num SMALLINT, tail_num VARCHAR(5),
  actual_elapsed_time SMALLINT, crs_elapsed_time SMALLINT,
  air_time SMALLINT, arr_delay SMALLINT, dep_delay SMALLINT,
  origin VARCHAR(3), dest VARCHAR(3), 
  distance SMALLINT, taxi_in SMALLINT, taxi_out SMALLINT, 
  cancelled SMALLINT, cancellation_code SMALLINT,
  diverted SMALLINT, carrier_delay SMALLINT, weather_delay SMALLINT, 
  nas_delay SMALLINT, security_delay SMALLINT, late_aircraft_delay SMALLINT)
ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
STORED AS RCFILE;

-- fill ORC flights table from text table
INSERT OVERWRITE TABLE flights SELECT * FROM flights_txt;

-- drop flights table with text output
DROP TABLE IF EXISTS flights_txt;