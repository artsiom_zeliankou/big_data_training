SELECT carrier, COUNT(*) as canceled_flights, concat_ws(', ' , collect_set(city)) as cities 
FROM (SELECT f.carrier_code as carrier, a.city as city FROM flights f
JOIN airports a ON f.origin = a.iata
WHERE f.cancelled > 0) tmp
GROUP BY carrier
ORDER BY canceled_flights DESC