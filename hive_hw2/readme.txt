### DataSet
- [Flying](http://stat-computing.org/dataexpo/2009/the-data.html ) (year 2007)
- [Airports](http://stat-computing.org/dataexpo/2009/supplemental-data.html) (Airports)
- [Carriers](http://stat-computing.org/dataexpo/2009/supplemental-data.html) (Carriers)

### Tasks:
0. Find all carriers who cancelled more than 1 flights during 2007, order them from biggest to lowest by number
of cancelled flights and list in each record all departure cities where cancellation happened. (Screenshot #1)
1. How many MR jobs where instanced for this query?

### Acceptance criteria
- Hive used
- Ambari or beeline clients used

### Expected outputs:
1. ZIP-ed folder with your scripts:
    1. queries
    2. table initialization scripts
2. Screenshot #1 with executed queries and result.
3. Answer to question in point 1(How many jobs...)


How to run:
1. Clone a repository
    git clone https://bitbucket.org/artsiom_zeliankou/big_data_training

2. Go to task directory
    cd hive_hw2

3. Run script with 2 parameters
    sh start.sh {userName} {password}

	where
	- userName - hadoop user name
	- password - hadoop user password

	example: sh start.sh raj_ops raj_ops