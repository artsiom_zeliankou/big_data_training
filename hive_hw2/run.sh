#!/usr/bin/env bash
#
echo "create external tables from input files"
hive -d airports=$1 -d carriers=$2 -d flights=$3 -f sql/create.sql
#
echo "Find all carriers who cancelled more than 1 flights during 2007, order them from biggest to lowest by number 
of cancelled flights and list in each record all departure cities where cancellation happened"
beeline -u jdbc:hive2:// -n $4 -p $5 -f sql/task.sql