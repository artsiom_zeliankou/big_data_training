This is a MapReduce program which takes a file as an input,
calculates amount of high-bid-priced (more than 250)** impression events by city
and print result where each city presented with its name rather than id
output is CSV file with rows as next: Gomel, 1142

How to run:
1. Clone a repository
    git clone https://bitbucket.org/artsiom_zeliankou/big_data_training

2. Go to task directory
    cd hadoop_hw3

3. Run script with 2 parameters
    sh start.sh {inputLogFile} {LIBJAR}

	where
	inputLogFile - name of input log file with a full path
	$LIBJAR - name of user agent utility jar archive with a full path

	example: sh start.sh ../../dataset/hadoop_hw3/imp.zip src/main/resources/UserAgentUtils.jar