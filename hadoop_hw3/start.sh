#!/usr/bin/env bash
#set variables
inputDir=/input_data/artsiom_zeliankou/hadoop/task3
outputDir=/output_data/artsiom_zeliankou/hadoop/task3
inputfile=$1
LIBJAR=$2
cities=${inputDir}/cities
biddings=${inputDir}/biddings

#prepare input directory
hdfs dfs -rm -r $inputDir
hdfs dfs -mkdir -p $inputDir
hdfs dfs -rm -r $outputDir
#
hdfs dfs -mkdir -p $cities $biddings
unzip -o $inputfile
hdfs dfs -copyFromLocal imp/city.* $cities
hdfs dfs -copyFromLocal imp/imp.* $biddings
rm -rf imp
#
mvn clean install
#
hadoop jar target/hadoop3.jar -libjars $LIBJAR $biddings $outputDir $cities
#
#send result to stdout
hdfs dfs -cat $outputDir/*