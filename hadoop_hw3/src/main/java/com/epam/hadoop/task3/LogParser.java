package com.epam.hadoop.task3;

import eu.bitwalker.useragentutils.OperatingSystem;
import eu.bitwalker.useragentutils.UserAgent;

public class LogParser {

    public static final int BIDDING_PRICE_POSITION = 19;
    public static final int CITY_ID_POSITION = 7;
    public static final int USER_AGENT_POSITION = 4;

    public static LogItem getParsedLogItem(String rawLogItem) {

        String[] items = rawLogItem.split("\t");

        String cityId =  items[CITY_ID_POSITION];
        int price = Integer.parseInt(items[BIDDING_PRICE_POSITION]);
        String operatingSystem = getOSName(items[USER_AGENT_POSITION]);
        return new LogItem(cityId, price, operatingSystem);
    }

    private static String getOSName(String userAgentInfo) {
        UserAgent userAgent = UserAgent.parseUserAgentString(userAgentInfo);
        OperatingSystem operatingSystem = userAgent.getOperatingSystem();
        return operatingSystem.getName();
    }
}
