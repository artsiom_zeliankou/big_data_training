package com.epam.hadoop.task3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class BiddingLogReducer extends Reducer<Text, Pair, Text, IntWritable> {

    private static final String CITIES_FILE_NAME_MATCHER = "city";
    private static final String UNDEFINED_CITY_NAME = "undefined";

    Map<String, String> citiesMap = new HashMap();
    Text cityName = new Text("");
    IntWritable result = new IntWritable();

    @Override
    protected void reduce(Text key, Iterable<Pair> values, Context context)
            throws IOException, InterruptedException {

        loadCities(context);

        String mappedCityName = citiesMap.get(key.toString());
        if (mappedCityName == null || mappedCityName.isEmpty() || mappedCityName.contains("null")) {
            cityName.set(UNDEFINED_CITY_NAME);
        } else {
            cityName.set(mappedCityName);
        }


        int amount = 0;
        Iterator it = values.iterator();
        while (it.hasNext()) {
            Object next = it.next();
            amount += 1;
        }
        result.set(amount);
        context.write(cityName, result);
    }

    private void loadCities(Context context) throws IOException, InterruptedException {
        URI[] cacheFiles = context.getCacheFiles();
        if (cacheFiles != null && cacheFiles.length > 0) {
            for (URI uri : cacheFiles) {
                if (uri.getPath().toLowerCase().contains(CITIES_FILE_NAME_MATCHER)){
                    processResource(uri, context);
                    return;
                }
            }
        }
    }

    private void processResource(URI uri, Context context) throws IOException {
        Path path = new Path(uri.getPath());
        FileSystem fs = FileSystem.get(context.getConfiguration());
        try (BufferedReader br = new BufferedReader(new InputStreamReader(fs.open(path)));) {
            String line;
            while ((line = br.readLine()) != null) {
                parseCity(line);
            }

        } catch (IOException ex) {
            System.err.println();

        }
    }

    private void parseCity(String line) {
        String[] items = line.split("\\s+");
        citiesMap.put(items[0], items[1]);
    }
}