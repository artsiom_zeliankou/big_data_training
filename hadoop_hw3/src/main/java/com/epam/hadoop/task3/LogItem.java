package com.epam.hadoop.task3;

public class LogItem {

    private String cityId;
    private int price;
    private String operatingSystem;

    public LogItem() {
        this.cityId = "";
        this.price = 0;
        this.operatingSystem = "";
    }

    public LogItem(String cityId, int price, String operatingSystem) {
        this.cityId = cityId;
        this.price = price;
        this.operatingSystem = operatingSystem;
    }

    public String getCityId() {
        return cityId;
    }

    public int getPrice() {
        return price;
    }

    public String getOperatingSystem() {
        return operatingSystem;
    }
}
