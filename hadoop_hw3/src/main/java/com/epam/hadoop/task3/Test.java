package com.epam.hadoop.task3;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import eu.bitwalker.useragentutils.UserAgent;

public class Test {

    public static final int BIDDING_PRICE_POSITION = 19;
    public static final int CITY_ID_POSITION = 7;
    public static final int USER_AGENT_POSITION = 4;

    private static final String DELIMITER = "\t";
    private static final String UNDEFINED_CITY_NAME = "undefined";

    private static Map<String, String> citiesMap = new HashMap<>();
    private static Map<String, Integer> citiesCounter = new HashMap<>();
    private static Map<String, Integer> osCounter = new HashMap<>();
    private static Map<String, Integer> deviceCounter = new HashMap<>();

    public static void main(String[] args) throws IOException {

        String rawLogItem = "a80fc47c7b544c78eb54a793e72a198e\t20131027170602323\t1\tCAV6UQBndoT\tMozilla/5.0 (Windows NT 5.1; rv:24.0) Gecko/20100101 Firefox/24.0\t10.237.95.*\t0\t0\t1\td2c2382e14b7d7de3a32218dba3192b1\t503afcab3679835909f87eedbc559b0\tnull\tmm_45472847_4168669_13664047\t950\t90\tNa\tNa\t0\t12628\t294\t287\tnull\t2261\t13866\n";
//        test190218(rawLogItem);

//        initCitiesMap();
        String fileName = "D://tmp/imp.20131027.txt";
        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            stream.forEach(Test::test190218);
        }
//        System.out.println("Cities total: " + citiesCounter.size() + "\n" + citiesCounter);
//        System.out.println("OS total: " + osCounter.size() + "\n" + osCounter);
//        System.out.println("Device total: " + deviceCounter.size() + "\n" + deviceCounter);
//        testTreemap();
    }

    private static void initCitiesMap() throws IOException {
        String cityName = "D://tmp/city.en.txt";
        try (Stream<String> stream = Files.lines(Paths.get(cityName))) {
            stream.forEach(Test::parseCity);
        }

    }

    private static void parseCity(String line) {
        String[] items = line.split("\\s+");
        citiesMap.put(items[0], items[1]);
    }

    private static void parseItem(String rawItem) {
        System.out.println(rawItem);
        String[] items = rawItem.split("\t");
                System.out.println(Arrays.toString(items));

        String cityId = items[CITY_ID_POSITION];
        String cityName;
        String mappedCityName = citiesMap.get(cityId);
        if (mappedCityName == null || mappedCityName.isEmpty() || mappedCityName.contains("null")) {
            cityName = UNDEFINED_CITY_NAME;
        } else {
            cityName = mappedCityName;
        }
        System.out.print("City: " + cityId + " [" + cityName + "] ");
        System.out.print("Price: " + items[BIDDING_PRICE_POSITION]);

        String userAgentString = items[USER_AGENT_POSITION];
        //        System.out.println("User Agent: " + userAgentString);
        UserAgent userAgent = UserAgent.parseUserAgentString(userAgentString);
        System.out.print(" Browser: " + userAgent.getBrowser());
        System.out.print(" OS: " + userAgent.getOperatingSystem().getName() + "\n");
        System.out.println(" Device: " + userAgent.getOperatingSystem().getDeviceType().getName() + "\n");

        incCitiesCounter(cityName);
        incOsCounter(userAgent.getOperatingSystem().getName());
        incDeviceCounter(userAgent.getOperatingSystem().getDeviceType().getName());
    }

    private static void incCitiesCounter(String cityName) {
        Integer actualCnt = citiesCounter.get(cityName);
        if (actualCnt != null) {
            citiesCounter.put(cityName, actualCnt + 1);
        } else {
            citiesCounter.put(cityName, 1);
        }
    }

    private static void incOsCounter(String osName) {
        Integer actualCnt = osCounter.get(osName);
        if (actualCnt != null) {
            osCounter.put(osName, actualCnt + 1);
        } else {
            osCounter.put(osName, 1);
        }
    }

    private static void incDeviceCounter(String deviceName) {
        Integer actualCnt = deviceCounter.get(deviceName);
        if (actualCnt != null) {
            deviceCounter.put(deviceName, actualCnt + 1);
        } else {
            deviceCounter.put(deviceName, 1);
        }
    }

    static NavigableMap<Integer, Set<String>> wordLengthMap = new TreeMap<>();

    private static void testTreemap() {

        String line = new String("There is a sub interface SortedMap that extends the map interface with order-based lookup methods and it has a sub interface NavigableMap that extends it even further. The standard implementation of this interface, TreeMap, allows you to sort entries either by natural ordering (if they implement the Comparable interface) or by a supplied Comparator .There is also the special case of LinkedHashMap, a HashMap implementation that stores the order in which keys are inserted. There is however no interface to back up this functionality, nor is there a direct way to access the last key. You can only do it through tricks such as using a List in between");

        StringTokenizer itr = new StringTokenizer(line);
        while (itr.hasMoreTokens()) {
            String token = itr.nextToken();
            fillWordsLengthMap(token.length(), token);
        }

        Map.Entry<Integer, Set<String>> lastEntry = wordLengthMap.lastEntry();
        System.out.println("Max length: " + lastEntry.getKey() + " words: " + lastEntry.getValue());

    }

    private static void fillWordsLengthMap(int length, String word) {
        if (wordLengthMap.get(length) == null) {
            Set<String> words = new HashSet<>();
            words.add(word);
            wordLengthMap.put(length, words);
        } else {
            wordLengthMap.get(length).add(word);
        }

    }

    private static void test190218(String test) {
        String pattern = "^\\w+\\t\\d+\\t\\d+\\t\\w+\\t((?:[^”])+)\\t[\\d]+.[\\d]+.[\\d]+.\\*?\\t\\d+\\t(\\d+)\\t\\d+\\t\\w+\\t\\w+\\t\\w+\\t\\w+\\t\\d+\\t\\d+\\t\\w+\\t\\w+\\t\\d+\\t\\w+\\t\\d+\\t\\d+\\t\\w+\\t\\d+\\t\\S+?";

        String pattern2 = "^\\w+\\t\\d+\\t\\d+\\t\\w+\\t((?:[^”])+)\\t[\\d]+.[\\d]+.[\\d]+.\\*?\\t\\d+\\t(\\d+)\\t\\d+\\t\\w+\\t\\w+\\t\\w+\\t\\w+\\t\\d+\\t\\d+\\t\\w+\\t\\w+\\t\\d+\\t\\w+\\t\\d+\\t\\d+\\t\\w+\\t\\d+\\t\\S+?";

        Pattern p2 = Pattern.compile(pattern2);
        Pattern p = Pattern.compile(pattern);
        Matcher matcher = p.matcher(test);
        if (matcher.matches()) {
            System.out.print(matcher.group(1) + "     ");
            UserAgent userAgent = UserAgent.parseUserAgentString(matcher.group(1));
            System.out.print(" Browser: " + userAgent.getBrowser());
            System.out.print(" OS: " + userAgent.getOperatingSystem().getName() + "\n");
            System.out.print(" Device: " + userAgent.getOperatingSystem().getDeviceType().getName() + "\n");
            System.out.println(matcher.group(2));
        }

    }

}
