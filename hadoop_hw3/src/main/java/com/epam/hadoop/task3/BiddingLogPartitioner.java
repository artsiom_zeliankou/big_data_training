package com.epam.hadoop.task3;

import java.util.HashMap;
import java.util.Map;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;

public class BiddingLogPartitioner extends Partitioner<Text, Pair> {

    private static Map<String, Integer> osMap = new HashMap<>();

    static {
        osMap.put("ANDROID", 0);
        osMap.put("WINDOWS", 1);
        osMap.put("MAC", 2);
        osMap.put("IOS", 3);
        osMap.put("BLACKBERRY", 4);
        osMap.put("SONY", 5);
        osMap.put("LINUX", 6);
        osMap.put("UNUNTU", 7);
        osMap.put("SYMBIAN", 8);
        osMap.put("MEEGO", 9);
        osMap.put("UNKNOWN", 10);
    }

    @Override
    public int getPartition(Text text, Pair pair, int numReduceTasks) {
        if (numReduceTasks == 0)
            return 0;

        String osName = pair.getOperatingSystem();
        for (String osPattern : osMap.keySet()) {
            if (osName.toUpperCase().contains(osPattern)) {
                return osMap.get(osPattern);
            }
        }
        return numReduceTasks - 1;
    }
}
