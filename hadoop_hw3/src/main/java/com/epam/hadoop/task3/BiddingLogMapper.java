package com.epam.hadoop.task3;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class BiddingLogMapper extends Mapper<LongWritable, Text, Text, Pair> {

    private static final int PRICE_MIN_VALUE = 250;
    private static final String DELIMITER = "\n";

    private Text cityId = new Text("");
    private Pair pair = new Pair();

    @Override
    protected void map(LongWritable key, Text value, Context context)
            throws IOException, InterruptedException {

        StringTokenizer itr = new StringTokenizer(value.toString(), DELIMITER);

        while (itr.hasMoreTokens()) {
            LogItem item = LogParser.getParsedLogItem(itr.nextToken());

            int price = item.getPrice();
            if (price > PRICE_MIN_VALUE) {
                cityId.set(item.getCityId());
                pair.setPrice(item.getPrice());
                pair.setOperatingSystem(item.getOperatingSystem());
                context.write(cityId, pair);
            }
        }
    }

}
