package com.epam.hadoop.task3;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.LazyOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class BiddingLogMRDriver extends Configured implements Tool {

    private static String K_V_SEPARATOR = ",";

    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new Configuration(), new BiddingLogMRDriver(), args);
        System.out.println("Result: " + res);
        System.exit(res);
    }

    public int run(String[] args) throws Exception {
        Configuration conf = getConf();
        conf.set(TextOutputFormat.SEPERATOR, K_V_SEPARATOR);

        if (args.length != 3) {
            System.out.println("3 args needed: {hdfsInputDir} {hdfsOutputDir} {lookupFileDir}");
            return 2;
        }

        String hdfsInputFileOrDirectory = args[0];
        String hdfsOutputFolder = args[1];
        String lookupFile = args[2];
        System.out.println("lookupFile: " + lookupFile);

        Job job = Job.getInstance(conf);

        loadfromCache(lookupFile, job);

        job.setNumReduceTasks(1);

        job.setJarByClass(getClass());
        job.setJobName(getClass().getName());

        job.setMapperClass(BiddingLogMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Pair.class);

        job.setReducerClass(BiddingLogReducer.class);

        job.setPartitionerClass(BiddingLogPartitioner.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        FileInputFormat.addInputPath(job, new Path(hdfsInputFileOrDirectory));
        LazyOutputFormat.setOutputFormatClass(job, TextOutputFormat.class);
        TextOutputFormat.setOutputPath(job, new Path(hdfsOutputFolder));

        return job.waitForCompletion(true) ? 0 : 1;
    }

    private static void loadfromCache(String lookupFilePath, Job job) {

        try {
            Path path = new Path(lookupFilePath);
            FileSystem fs = FileSystem.get(job.getConfiguration());
            RemoteIterator<LocatedFileStatus> itr = fs.listFiles(path, true);

            while (itr.hasNext()) {
                LocatedFileStatus f = itr.next();

                if (!f.isDirectory() && f.getPath().getName().toLowerCase().contains("city")) {
                    job.addCacheFile(f.getPath().toUri());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
