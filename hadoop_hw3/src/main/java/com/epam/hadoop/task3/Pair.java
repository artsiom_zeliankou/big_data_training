package com.epam.hadoop.task3;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Objects;

import org.apache.hadoop.io.Writable;

public class Pair implements Writable {

    private int price;
    private String operatingSystem;

    public Pair(int price, String operatingSystem) {
        this.price = price;
        this.operatingSystem = operatingSystem;
    }

    public Pair() {
        this(0, "");
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getOperatingSystem() {
        return operatingSystem;
    }

    public void setOperatingSystem(String operatingSystem) {
        this.operatingSystem = operatingSystem;
    }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeLong(price);
        out.writeBytes(operatingSystem);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        price = in.readInt();
        operatingSystem = in.readLine();
    }

    @Override public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Pair))
            return false;
        Pair pair = (Pair) o;
        return price == pair.price &&
                Objects.equals(operatingSystem, pair.operatingSystem);
    }

    @Override public int hashCode() {
        return Objects.hash(price, operatingSystem);
    }
}
