package com.epam.hadoop.task3;

import java.util.List;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BiddingLogMRDriverTest {

    private MapReduceDriver<LongWritable, Text, Text, Pair, Text, IntWritable> mapReduceDriver;

    @Before
    public void setup() {
        BiddingLogMapper mapper = new BiddingLogMapper();
        BiddingLogReducer reducer = new BiddingLogReducer();
        mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
    }

    @Test
    public void testMapReduce() throws Exception {

        Text value1 = new Text(
                "f9f5d9f335ca8d9fc37e736567bcfc1\t20131019163900673\t1\tDAJDulBMexw1\tMozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)\t113.90.24.*\t216\t219\t1\t20fc675468712705dbf5d3eda94126da\t71da4a502bbe17528b773fc681533c2\tnull\tmm_10982364_973726_8930541\t300\t250\tFourthView\tNa\t0\t7323\t294\t27\tnull\t2259\tnull");

        Text value2 = new Text(
                "f9f5d9f335ca8d9fc37e736567bcfc1\t20131019163900673\t1\tDAJDulBMexw1\tMozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)\t113.90.24.*\t216\t219\t1\t20fc675468712705dbf5d3eda94126da\t71da4a502bbe17528b773fc681533c2\tnull\tmm_10982364_973726_8930541\t300\t250\tFourthView\tNa\t0\t7323\t204\t27\tnull\t2259\tnull");
        mapReduceDriver.withInput(new LongWritable(0), new Text(value1));
        mapReduceDriver.withInput(new LongWritable(1), new Text(value2));

        List<org.apache.hadoop.mrunit.types.Pair<Text, IntWritable>> result = mapReduceDriver.run();

        org.apache.hadoop.mrunit.types.Pair expected = new org.apache.hadoop.mrunit.types.Pair<>(new Text("undefined"), new IntWritable(1));

        Assert.assertTrue(result != null);
        Assert.assertFalse(result.isEmpty());
        Assert.assertTrue(result.size() == 1);
        Assert.assertEquals(result.get(0), expected);
    }
}
