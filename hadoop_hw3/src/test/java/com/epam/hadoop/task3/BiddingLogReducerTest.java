package com.epam.hadoop.task3;

import java.io.IOException;
import java.util.Arrays;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

public class BiddingLogReducerTest {

    private ReduceDriver<Text, Pair, Text, IntWritable> reduceDriver;

    @Before
    public void setup() {
        BiddingLogReducer reducer = new BiddingLogReducer();
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
    }

    @Test
    public void reduceAndCalculateInputValuesWithUndefinedCityName() throws IOException, InterruptedException {
        reduceDriver.withInput(new Text("219"), Arrays.asList(new Pair(100, ""), new Pair(2, "")))
                .withOutput(new Text("undefined"), new IntWritable(2))
                .runTest();
    }
}
