package com.epam.hadoop.task3;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.junit.Before;
import org.junit.Test;

public class BiddingLogMapperTest {

    private MapDriver<LongWritable, Text, Text, Pair> mapDriver;

    @Before
    public void setUp() throws Exception {
        BiddingLogMapper mapper = new BiddingLogMapper();
        mapDriver = MapDriver.newMapDriver(mapper);
    }

    @Test
    public void parseTwoInputsWithSingleOutput() throws IOException, InterruptedException {
        Text value1 = new Text(
                "f9f5d9f335ca8d9fc37e736567bcfc1\t20131019163900673\t1\tDAJDulBMexw1\tMozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)\t113.90.24.*\t216\t219\t1\t20fc675468712705dbf5d3eda94126da\t71da4a502bbe17528b773fc681533c2\tnull\tmm_10982364_973726_8930541\t300\t250\tFourthView\tNa\t0\t7323\t294\t27\tnull\t2259\tnull");

        Text value2 = new Text(
                "f9f5d9f335ca8d9fc37e736567bcfc1\t20131019163900673\t1\tDAJDulBMexw1\tMozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)\t113.90.24.*\t216\t219\t1\t20fc675468712705dbf5d3eda94126da\t71da4a502bbe17528b773fc681533c2\tnull\tmm_10982364_973726_8930541\t300\t250\tFourthView\tNa\t0\t7323\t204\t27\tnull\t2259\tnull");

        mapDriver.withInput(new LongWritable(1), value1)
                .withInput(new LongWritable(1), value2)
                .withOutput(new Text("219"), new Pair(294, "Windows 7"))
                .runTest();
    }

}
