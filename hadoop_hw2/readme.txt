This is a MapReduce program which takes a path to log file as an input,
count average bytes per request by IP and total bytes by IP and
output is CSV file with rows as next: IP,175.5,109854

How to run:
1. Clone a repository
    git clone https://bitbucket.org/artsiom_zeliankou/big_data_training

2. Go to task directory
    cd hadoop_hw2

3. Run script with 2 parameters
    sh start.sh {inputPath} {LIBJAR}
	
	where 
	$inputPath - path to input log files
	$LIBJAR - name of user agent utility jar archive with a full path
	
	example: sh start.sh src/main/resources src/main/resources/UserAgentUtils.jar

4. In order to set output format as "text", uncomment  line 65, 66, 67 in AccessLogMRDriver
        and comment lines 71, 72, 73, 74