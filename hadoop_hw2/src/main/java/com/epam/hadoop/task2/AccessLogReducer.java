package com.epam.hadoop.task2;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class AccessLogReducer extends Reducer<Text, LongPair, Text, LongDoublePair> {

    private LongDoublePair result = new LongDoublePair();

    @Override
    protected void reduce(Text key, Iterable<LongPair> values, Context context)
            throws IOException, InterruptedException {

        long sum = 0L;
        long cnt = 0L;

        for (LongPair val : values) {
            sum += val.getTotalBytes();
            cnt += val.getCount();
        }

        double avgBytes = (double) sum / cnt;
        result.setTotalBytes(sum);
        result.setAvgBytes(round(avgBytes));
        context.write(key, result);
    }

    private double round(double target) {
        return Math.round(target * 10.0) / 10.0;
    }

}