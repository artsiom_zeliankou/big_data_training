package com.epam.hadoop.task2;

public class LogItem {

    private String ipAddress;
    private int bytesAmount;
    private String userAgent;

    public LogItem() {
        this.ipAddress = "";
        this.bytesAmount = 0;
        this.userAgent = "";
    }

    public LogItem(String ipAddress, int bytesAmount, String userAgent) {
        this.ipAddress = ipAddress;
        this.bytesAmount = bytesAmount;
        this.userAgent = userAgent;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public int getBytesAmount() {
        return bytesAmount;
    }

    public String getUserAgent() {
        return userAgent;
    }
}
