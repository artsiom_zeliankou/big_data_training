package com.epam.hadoop.task2;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Objects;

import org.apache.hadoop.io.Writable;

public class LongDoublePair implements Writable {

    public long totalBytes;
    public double avgBytes;

    public LongDoublePair(long totalBytes, double avgBytes) {
        this.totalBytes = totalBytes;
        this.avgBytes = avgBytes;
    }

    public LongDoublePair() {
        this(0L, 0.0);
    }

    public long getTotalBytes() {
        return totalBytes;
    }

    public void setTotalBytes(long totalBytes) {
        this.totalBytes = totalBytes;
    }

    public double getAvgBytes() {
        return avgBytes;
    }

    public void setAvgBytes(double avgBytes) {
        this.avgBytes = avgBytes;
    }

    public void write(DataOutput out) throws IOException {
        out.writeLong(totalBytes);
        out.writeDouble(avgBytes);
    }

    public void readFields(DataInput in) throws IOException {
        totalBytes = in.readLong();
        avgBytes = in.readDouble();
    }

    public String toString() {
        return Double.toString(avgBytes)  + "," + Long.toString(totalBytes);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof LongDoublePair))
            return false;
        LongDoublePair that = (LongDoublePair) o;
        return totalBytes == that.totalBytes &&
                Double.compare(that.avgBytes, avgBytes) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(totalBytes, avgBytes);
    }
}
