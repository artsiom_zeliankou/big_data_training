package com.epam.hadoop.task2;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class AccessLogCombiner extends Reducer<Text, LongPair, Text, LongPair> {

    private LongPair outputValue = new LongPair();

    public void reduce(Text key, Iterable<LongPair> values,Context context) throws IOException, InterruptedException {

        long sum = 0L;
        long cnt = 0L;

        for (LongPair val : values) {
            sum +=  val.getTotalBytes();
            cnt +=  val.getCount();
        }

        outputValue.setTotalBytes(sum);
        outputValue.setCount(cnt);
        context.write(key, outputValue);
    }
}
