package com.epam.hadoop.task2;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class AccessLogMapper extends Mapper<LongWritable, Text, Text, LongPair> {

    static final String COUNTER_GROUP = "DynamicCounter";
    private static final String DELIMITER = "\n";
    private static final String INVALIG_LOG_ITEM = "Invalid Log Item";

    private Text ipAddress = new Text();
    private LongPair pair = new LongPair();

    @Override
    protected void map(LongWritable key, Text value, Context context)
            throws IOException, InterruptedException {

        StringTokenizer itr = new StringTokenizer(value.toString(), DELIMITER);

        while (itr.hasMoreTokens()) {
            LogItem item = LogItemParser.getParsedLogItem(itr.nextToken());

            if (item.getIpAddress().isEmpty()) {
                incCounter(context, INVALIG_LOG_ITEM);
            } else {
                ipAddress.set(item.getIpAddress());
                pair.setTotalBytes(item.getBytesAmount());
                pair.setCount(1);
                context.write(ipAddress, pair);
                incCounter(context, item.getUserAgent());
            }
        }
    }

    private void incCounter(Context context, String counterName) {
        context.getCounter(COUNTER_GROUP, counterName).increment(1);
    }

}
