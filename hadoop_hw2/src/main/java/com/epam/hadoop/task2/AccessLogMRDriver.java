package com.epam.hadoop.task2;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.compress.DefaultCodec;
import org.apache.hadoop.io.compress.SnappyCodec;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.CounterGroup;
import org.apache.hadoop.mapreduce.Counters;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.LazyOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class AccessLogMRDriver extends Configured implements Tool {

    private static String K_V_SEPARATOR = ",";
    private static final Log LOG = LogFactory.getLog(AccessLogMRDriver.class);

    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new Configuration(), new AccessLogMRDriver(), args);
        LOG.info("Result code: " + res);
        System.exit(res);
    }

    public int run(String[] args) throws Exception {
        Configuration conf = getConf();

        if (args.length != 2) {
            LOG.error("2 args needed: {hdfsInputDir} {hdfsOutputDir}");
            return 2;
        }

        String hdfsInputFileOrDirectory = args[0];
        String hdfsOutputFolder = args[1];

        Job job = Job.getInstance(conf);

        job.setJarByClass(getClass());
        job.setJobName(getClass().getName());

        job.setMapperClass(AccessLogMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(LongPair.class);

//        job.setSortComparatorClass(LongPairComparator.class);

        job.setCombinerClass(AccessLogCombiner.class);
        job.setReducerClass(AccessLogReducer.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(LongDoublePair.class);

        FileInputFormat.addInputPath(job, new Path(hdfsInputFileOrDirectory));
        //        TextOutputFormat.setOutputPath(job, new Path(hdfsOutputFolder));
        //        LazyOutputFormat.setOutputFormatClass(job, TextOutputFormat.class);
        //        conf.set(TextOutputFormat.SEPERATOR, K_V_SEPARATOR);


        // Save output as Sequence file compressed with Snappy (key is IP, and value is custom object for avg and total size)
        SequenceFileOutputFormat.setOutputPath(job, new Path(hdfsOutputFolder));
        SequenceFileOutputFormat.setCompressOutput(job, true);
        SequenceFileOutputFormat.setOutputCompressorClass(job, SnappyCodec.class);
        SequenceFileOutputFormat.setOutputCompressionType(job, SequenceFile.CompressionType.BLOCK);

        boolean jobCompleted = job.waitForCompletion(true);
        printJobCounters(job);
        return jobCompleted ? 0 : 1;

    }

    private void printJobCounters(Job job) throws IOException {
        Counters counters = job.getCounters();
        CounterGroup group = counters.getGroup(AccessLogMapper.COUNTER_GROUP);
        System.out.println("Dynamic counters info:");
        for (Counter counter : group) {
            System.out.println(counter.getName() + ": " + counter.getValue());
        }
    }

}
