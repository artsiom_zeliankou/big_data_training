package com.epam.hadoop.task2;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Objects;

import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;

public class LongPair implements Writable, WritableComparable<LongPair> {

    public long totalBytes;
    public long count;

    public LongPair(long totalBytes, long count) {
        this.totalBytes = totalBytes;
        this.count = count;
    }

    public LongPair() {
        this(0L, 0);
    }

    public long getTotalBytes() {
        return totalBytes;
    }

    public void setTotalBytes(long totalBytes) {
        this.totalBytes = totalBytes;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public void write(DataOutput out) throws IOException {
        out.writeLong(totalBytes);
        out.writeLong(count);
    }

    public void readFields(DataInput in) throws IOException {
        totalBytes = in.readLong();
        count = in.readLong();
    }

    public String toString() {
        return Long.toString(totalBytes) + ", " + Long.toString(count);
    }

    @Override public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof LongPair))
            return false;
        LongPair that = (LongPair) o;
        return totalBytes == that.totalBytes &&
                count == that.count;
    }

    @Override public int hashCode() {
        return Objects.hash(totalBytes, count);
    }

    @Override
    public int compareTo(LongPair o) {
        int cmp = Long.compare(totalBytes, o.totalBytes);
        if(0 != cmp)
            return cmp;
        return Long.compare(count, o.count);
    }
}
