package com.epam.hadoop.task2;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


import eu.bitwalker.useragentutils.UserAgent;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class LogItemParser {

    private static final int FIELDS_NUMBER = 9;
    private static final int IP_ADDRESS_POSITION = 1;
    private static final int BYTES_AMOUNT_POSITION = 7;
    private static final int USER_AGENT_AMOUNT_POSITION = 9;
    private static final String PATTERN = "^([\\w]+) (\\S+) (\\S+) \\[([\\w:/]+\\s[+\\-]\\d{4})\\] \"(.+?)\" (\\d{3}) (\\d+) \"([^\"]+)\" \"([^\"]+)\"";

    private static final Log LOG = LogFactory.getLog(LogItemParser.class);

    public static LogItem getParsedLogItem(String rawLogItem) {

        Pattern p = Pattern.compile(PATTERN);
        Matcher matcher = p.matcher(rawLogItem);
        if (!matcher.matches() ||  FIELDS_NUMBER != matcher.groupCount()) {
            LOG.error("Bad log entry: " + rawLogItem);
            return new LogItem();
        }

        String ipAddress = matcher.group(IP_ADDRESS_POSITION);
        int bytesAmount = Integer.parseInt(matcher.group(BYTES_AMOUNT_POSITION));
        String userAgent = geBrowserName(matcher.group(USER_AGENT_AMOUNT_POSITION));
        return new LogItem(ipAddress, bytesAmount, userAgent);
    }

    private static String geBrowserName(String userAgentInfo) {
        return UserAgent.parseUserAgentString(userAgentInfo).getBrowser().getName();
    }
}
