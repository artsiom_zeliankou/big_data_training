package com.epam.hadoop.task2;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.junit.Before;
import org.junit.Test;

public class AccessLogMapperTest {

    private MapDriver<LongWritable, Text, Text, LongPair> mapDriver;

    @Before
    public void setUp() throws Exception {
        AccessLogMapper mapper = new AccessLogMapper();
        mapDriver = MapDriver.newMapDriver(mapper);
    }

    @Test
    public void parseValidInputLog() throws IOException, InterruptedException {
        Text value = new Text(
                "ip1 - - [24/Apr/2011:04:06:01 -0400] \"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 40028 \"-\" \"Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)\"");
        mapDriver.withInput(new LongWritable(1), value)
                .withOutput(new Text("ip1"), new LongPair(40028, 1))
                .runTest();
    }

    @Test
    public void parseInValidInputLog() throws IOException, InterruptedException {
        Text value = new Text(
                "ip1 - - [24/Apr/2011:04:06:01 -0400] \"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 40028 \"-\" \"Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)\" [24/Apr/2011:04:06:01 -0400] \"GET");
        mapDriver.withInput(new LongWritable(1), value)
                .runTest();
    }

}
