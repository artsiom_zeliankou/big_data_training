package com.epam.hadoop.task2;

import java.util.List;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.types.Pair;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AccessLogMRDriverTest {

    private MapReduceDriver<LongWritable, Text, Text, LongPair, Text, LongDoublePair> mapReduceDriver;

    @Before
    public void setup() {
        AccessLogMapper mapper = new AccessLogMapper();
        AccessLogReducer reducer = new AccessLogReducer();
        AccessLogCombiner combiner = new AccessLogCombiner();
        mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer, combiner);
    }

    @Test
    public void testMapReduce() throws Exception {

        String line1 = "ip1 - - [24/Apr/2011:04:06:01 -0400] "
                + "\"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 1000 \"-\" "
                + "\"Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)\"";

        String line2 = "ip1 - - [24/Apr/2011:04:06:01 -0400] "
                + "\"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 3000 \"-\" "
                + "\"Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)\"";


        String line3 = "ip2 - - [24/Apr/2011:04:06:01 -0400] "
                + "\"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 111111 \"-\" "
                + "\"Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)\"";

        String line4 = "ip2 - - [24/Apr/2011:04:06:01 -0400] "
                + "\"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 111111 \"-\" "
                + "\"Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)\"";

        String line5 = "ip3 - - [24/Apr/2011:04:06:01 -0400] "
                + "\"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 2018 \"-\" "
                + "\"Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)\"";

        String emptyLine = "";
        String invalidLine = "ip3 - - [24/Apr/2011:04:06:01 -0400] "
                + "\"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 40028 \"-\" "
                + "\"Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)\""
                + "knett45tj45t  5465465436354 kkg[][6]6[546[6[4]6[  664]56[]6[]4[";

        mapReduceDriver.withInput(new LongWritable(0), new Text(line1));
        mapReduceDriver.withInput(new LongWritable(1), new Text(line2));
        mapReduceDriver.withInput(new LongWritable(2), new Text(line3));
        mapReduceDriver.withInput(new LongWritable(3), new Text(line4));
        mapReduceDriver.withInput(new LongWritable(4), new Text(line5));
        mapReduceDriver.withInput(new LongWritable(5), new Text(emptyLine));
        mapReduceDriver.withInput(new LongWritable(5), new Text(invalidLine));

        List<Pair<Text, LongDoublePair>> result = mapReduceDriver.run();

        Pair expected1 = new Pair<>(new Text("ip1"), new LongDoublePair(4000, 2000.0));
        Pair expected2 = new Pair<>(new Text("ip2"), new LongDoublePair(222222, 111111.0));
        Pair expected3 = new Pair<>(new Text("ip3"), new LongDoublePair(2018, 2018.0));

        Assert.assertTrue(result != null);
        Assert.assertFalse(result.isEmpty());
        Assert.assertTrue(result.size() == 3);
        Assert.assertTrue(result.contains(expected1));
        Assert.assertTrue(result.contains(expected2));
        Assert.assertTrue(result.contains(expected3));
    }

}
