package com.epam.hadoop.task2;

import java.io.IOException;
import java.util.Arrays;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

public class AccessLogReducerTest {

    private ReduceDriver<Text, LongPair, Text, LongDoublePair> reduceDriver;

    @Before
    public void setup() {
        AccessLogReducer reducer = new AccessLogReducer();
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
    }

    @Test
    public void reduceAndCalculateInputValue() throws IOException, InterruptedException {
        reduceDriver.withInput(new Text("ip1"), Arrays.asList(new LongPair(1000, 1), new LongPair(3000, 1)))
                .withOutput(new Text("ip1"), new LongDoublePair(4000, 2000.0))
                .runTest();
    }
}
