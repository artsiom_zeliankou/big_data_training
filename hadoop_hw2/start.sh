#!/usr/bin/env bash
#set variables
inputDir=/input_data/artsiom_zeliankou/hadoop/task2
outputDir=/output_data/artsiom_zeliankou/hadoop/task2
inputPath=$1
LIBJAR=$2
#prepare working directories
hdfs dfs -rm -r $inputDir
hdfs dfs -mkdir -p $inputDir
hdfs dfs -rm -r $outputDir
#
hdfs dfs -copyFromLocal ${inputPath}/* ${inputDir}/
#
mvn clean install
#
hadoop jar target/hadoop2.jar -libjars $LIBJAR $inputDir $outputDir
#
hdfs dfs -text $outputDir/*