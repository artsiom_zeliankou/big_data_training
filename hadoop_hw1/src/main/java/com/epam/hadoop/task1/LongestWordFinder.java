package com.epam.hadoop.task1;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class LongestWordFinder extends Configured implements Tool {

    private static final Log LOG = LogFactory.getLog(LongestWordFinder.class);

    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new Configuration(), new LongestWordFinder(), args);
        LOG.info("Result code: " + res);
        System.exit(res);
    }

    public int run(String[] args) throws Exception{
        Configuration conf = getConf();

        if (args.length != 2) {
            LOG.error("2 args needed: {hdfsInputDir} {hdfsOutputDir}");
            return 2;
        }

        String hdfsInputFileOrDirectory = args[0];
        String hdfsOutputFolder = args[1];
        Job job = Job.getInstance(conf);

        job.setJarByClass(getClass());
        job.setJobName(getClass().getName());

        job.setMapperClass(LongestWordMapper.class);
        job.setCombinerClass(LongestWordCombiner.class);
        job.setReducerClass(LongestWordReducer.class);

        job.setOutputKeyClass(IntWritable.class);
        job.setOutputValueClass(Text.class);

        FileInputFormat.addInputPath(job, new Path(hdfsInputFileOrDirectory));
        FileOutputFormat.setOutputPath(job, new Path(hdfsOutputFolder));

        return job.waitForCompletion(true) ? 0 : 1;
    }

}
