package com.epam.hadoop.task1;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class LongestWordCombiner extends Reducer<IntWritable, Text, IntWritable, Text> {

    private static final String MULTI_VALUES_SEPARATOR = ",";

    private Set<String> inputValuesSet = new HashSet<>();
    private Text outputValue = new Text();

    protected void reduce(IntWritable key, Iterable<Text> values, Reducer.Context context)
            throws IOException, InterruptedException {

        // put all words to set to exclude duplications
        for (Text text : values) {
            inputValuesSet.add(text.toString());
        }

        StringBuilder sb = new StringBuilder();

        for (String word : inputValuesSet) {
            sb.append(word).append(MULTI_VALUES_SEPARATOR);
        }
        outputValue.set(sb.toString());
        context.write(key, outputValue);

    }

}
