package com.epam.hadoop.task1;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.hadoop.io.ArrayWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class LongestWordReducer extends Reducer<IntWritable, Text, IntWritable, Text> {

    private int maxSize;
    private Set<String> resultSet = new HashSet<String>();

    @Override
    protected void reduce(IntWritable key, Iterable<Text> value, Context context)
            throws IOException, InterruptedException {

        if (key.get() > maxSize) {
            maxSize = key.get();
            resultSet.clear();
            Iterator<Text> it = value.iterator();
            while(it.hasNext()) {
                resultSet.add(it.next().toString());
            }
        } else
            if (key.get() == maxSize) {
                Iterator<Text> it = value.iterator();
                while(it.hasNext()) {
                    resultSet.add(it.next().toString());
                }
        }
    }

    protected void cleanup(Context context) throws IOException, InterruptedException {
        context.write(new IntWritable(maxSize), new Text(Arrays.toString(resultSet.toArray())));
    }
}