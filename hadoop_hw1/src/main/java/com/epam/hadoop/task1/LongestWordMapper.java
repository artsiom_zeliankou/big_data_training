package com.epam.hadoop.task1;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class LongestWordMapper extends Mapper<LongWritable, Text, IntWritable, Text> {

    private static final String SEPARATOR_SYMBOLS = " \t\n\r\f,.:;[{}]";

    private int prevMaxLength;

    private Text outputWord = new Text();
    private IntWritable outputLength = new IntWritable();

    @Override
    protected void map(LongWritable key, Text value, Context context)
            throws IOException, InterruptedException {

        StringTokenizer itr = new StringTokenizer(value.toString(), SEPARATOR_SYMBOLS);
        while (itr.hasMoreTokens()) {

            String word = itr.nextToken();
            int length = word.length();

            // write result to output only if
            // token length >= max length from previous lines
            if (length >= prevMaxLength) {
                outputLength.set(length);
                outputWord.set(word);
                context.write(outputLength, outputWord);
            }

            // set new threshold
            if (length > prevMaxLength) {
                prevMaxLength = length;
            }
        }

    }

}
