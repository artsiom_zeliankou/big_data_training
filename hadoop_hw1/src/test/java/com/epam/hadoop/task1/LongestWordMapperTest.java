package com.epam.hadoop.task1;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.junit.Before;
import org.junit.Test;

public class LongestWordMapperTest {

    private MapDriver<LongWritable, Text, IntWritable, Text> mapDriver;

    @Before
    public void setUp() throws Exception {
        LongestWordMapper mapper = new LongestWordMapper();
        mapDriver = MapDriver.newMapDriver(mapper);
    }

    @Test
    public void splitValidRecordIntoTokens() throws IOException, InterruptedException {
        Text value = new Text("public class LongestWordMapperTest");
        mapDriver.withInput(new LongWritable(1), value)
                .withOutput(new IntWritable(6), new Text("public"))
                .withOutput(new IntWritable(21), new Text("LongestWordMapperTest"))
                .runTest();
    }

    @Test
    public void splitValidRecordIntoTokensWith2DifferentLongestWords() throws IOException, InterruptedException {
        Text value = new Text(
                "public class LongestWordMapperTest1 Download the latest version of MRUnit jar from Apache website Download the latest version of MRUnit jar from Apache website LongestWordMapperTest2");
        mapDriver.withInput(new LongWritable(1), value)
                .withOutput(new IntWritable(6), new Text("public"))
                .withOutput(new IntWritable(22), new Text("LongestWordMapperTest1"))
                .withOutput(new IntWritable(22), new Text("LongestWordMapperTest2"))
                .runTest();
    }

    @Test
    public void splitValidRecordIntoTokensWith2EqualLongestWords() throws IOException, InterruptedException {
        Text value = new Text(
                "public class LongestWordMapperTest Download the latest version of MRUnit jar from Apache website Download the latest version of MRUnit jar from Apache website LongestWordMapperTest");
        mapDriver.withInput(new LongWritable(1), value)
                .withOutput(new IntWritable(6), new Text("public"))
                .withOutput(new IntWritable(21), new Text("LongestWordMapperTest"))
                .withOutput(new IntWritable(21), new Text("LongestWordMapperTest"))
                .runTest();
    }

    @Test
    public void recordWithSingleWordIsValid() throws IOException, InterruptedException {
        Text value = new Text("one");
        mapDriver.withInput(new LongWritable(1), value)
                .withOutput(new IntWritable(3), new Text("one"))
                .runTest();
    }

    @Test
    public void recordWithEmptyLineOutputsNothing() throws IOException, InterruptedException {
        Text value = new Text("");
        mapDriver.withInput(new LongWritable(), value)
                .runTest();
    }

}
