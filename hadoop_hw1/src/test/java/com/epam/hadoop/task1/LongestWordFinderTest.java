package com.epam.hadoop.task1;

import java.util.List;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.types.Pair;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class LongestWordFinderTest {

    private MapReduceDriver<LongWritable, Text, IntWritable, Text, IntWritable, Text> mapReduceDriver;

    @Before
    public void setup() {
        LongestWordMapper mapper = new LongestWordMapper();
        LongestWordReducer reducer = new LongestWordReducer();
        mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
    }

    @Test
    public void testMapReduce() throws Exception {

        String line1 = "Loremipsumdolor sit amet, consecteturadipisicingelit, seddo eiusmodtemporincididuntutlabore";
        String line2 = "et doloremagna aliqua. Utenimad minim veniam, quisnostrudexercitation ullamcolaborisnisi ute";
        mapReduceDriver.withInput(new LongWritable(0), new Text(line1));
        mapReduceDriver.withInput(new LongWritable(1), new Text(line2));

        List<Pair<IntWritable, Text>> result = mapReduceDriver.run();

        Pair expected = new Pair<IntWritable, Text>(new IntWritable(31), new Text("[eiusmodtemporincididuntutlabore]"));

        Assert.assertTrue(result != null);
        Assert.assertFalse(result.isEmpty());
        Assert.assertTrue(result.size() == 1);
        Assert.assertEquals(expected, result.get(0));
    }

}
