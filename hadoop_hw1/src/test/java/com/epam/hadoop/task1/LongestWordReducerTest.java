package com.epam.hadoop.task1;

import java.io.IOException;
import java.util.Arrays;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

public class LongestWordReducerTest {

    private ReduceDriver<IntWritable, Text, IntWritable, Text> reduceDriver;

    @Before
    public void setup() {
        LongestWordReducer reducer = new LongestWordReducer();
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
    }

    @Test
    public void splitValidRecordIntoTokens() throws IOException, InterruptedException {
        reduceDriver.withInput(new IntWritable(5), Arrays.asList(new Text("apache"), new Text("maven")))
                .withOutput(new IntWritable(5), new Text("[apache, maven]"))
                .runTest();
    }
}
