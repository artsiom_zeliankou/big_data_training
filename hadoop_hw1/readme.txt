This is a MapReduce program which takes folder name with a text files as an input, searches the longest word among them and writes result
to the output in format [number of symbols] [longest words]

Limitations to input file:  words in file should be separated by space, tab or comma.

How to run:
1. Clone repository into your HDP Sandbox
    git clone https://bitbucket.org/artsiom_zeliankou/big_data_training

2. Go to task directory
    cd hadoop_hw1

3. Run script with one parameter which represents a directory with input files
    sh start.sh {dataDir}

    example: sh start.sh ../../dataset/hadoop_hw1