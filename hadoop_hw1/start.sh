#!/usr/bin/env bash
#set variables
inputDir=/input_data/artsiom_zeliankou/hadoop/task1
outputDir=/output_data/artsiom_zeliankou/hadoop/task1
dataDir=$1
#
#prepare working directories
hdfs dfs -rm -r $inputDir
hdfs dfs -mkdir -p $inputDir
hdfs dfs -rm -r $outputDir
#
#copy input files to HDFS
hdfs dfs -copyFromLocal ${dataDir}/* ${inputDir}/
#
mvn clean install
#
hadoop jar target/hadoop1.jar $inputDir $outputDir
#
echo "Result: [number of symbols] [longest words]"
hdfs dfs -cat $outputDir/*