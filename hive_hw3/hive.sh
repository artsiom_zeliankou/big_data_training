#!/usr/bin/env bash
#
echo "===> create external tables from input files"
hive -d cities=$1 -d logs=$2 -f sql/create_tables.sql
#
echo "===> create UDF"
hive -f sql/create_udf.sql
#
echo "===> perform task"
#beeline -u jdbc:hive2:// -n 'hadoop' -p 'password' -f task3.sql
hive -f sql/task3.sql