ADD JAR hdfs:///input_data/artsiom_zeliankou/hive/task3/task3-1.0-SNAPSHOT-jar-with-dependencies.jar;
DROP FUNCTION IF EXISTS uparser;
CREATE FUNCTION uparser AS 'com.epam.hive.task3.UserAgentParser' USING jar 'hdfs:///input_data/artsiom_zeliankou/hive/task3/task3-1.0-SNAPSHOT-jar-with-dependencies.jar';