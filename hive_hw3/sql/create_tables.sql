--  CITIES
-- create external cities with text output
DROP TABLE IF EXISTS cities_txt;
CREATE EXTERNAL TABLE IF NOT EXISTS cities_txt(
  id STRING, name STRING)
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION "${cities}";

-- create external cities table with ORC output
DROP TABLE IF EXISTS cities;
CREATE TABLE IF NOT EXISTS cities(
   id STRING, name STRING)
ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
STORED AS RCFILE;

-- fill ORC cities table from text table
INSERT OVERWRITE TABLE cities SELECT * FROM cities_txt;

-- drop cities table with text output
DROP TABLE IF EXISTS cities_txt;



--  LOGS
-- create external logs table with text output
DROP TABLE IF EXISTS logs_txt;
CREATE EXTERNAL TABLE IF NOT EXISTS logs_txt(
  user_agent STRING, city_id STRING)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.RegexSerDe'
WITH SERDEPROPERTIES (
  'input.regex'='^\\w+\\t\\d+\\t\\d+\\t\\w+\\t((?:[^”])+)\\t[\\d]+.[\\d]+.[\\d]+.\\*?\\t\\d+\\t(\\d+)\\t\\d+\\t\\w+\\t\\w+\\t\\w+\\t\\w+\\t\\d+\\t\\d+\\t\\w+\\t\\w+\\t\\d+\\t\\w+\\t\\d+\\t\\d+\\t\\w+\\t\\d+\\t\\S+?',
  'output.format.string'='%1$s %2$s'
)
STORED AS TEXTFILE
LOCATION "${logs}";

-- create external logs table with ORC output
DROP TABLE IF EXISTS logs;
CREATE TABLE IF NOT EXISTS logs(
  user_agent STRING, city_id STRING)
ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
STORED AS RCFILE;

-- fill ORC logs table from text table
INSERT OVERWRITE TABLE logs SELECT * FROM logs_txt;

-- drop logs table with text output
DROP TABLE IF EXISTS logs_txt;


-- create table with separated user agent values
DROP TABLE IF EXISTS logs2;
CREATE TABLE IF NOT EXISTS logs2(
  city_id STRING, device STRING, browser STRING, os STRING)
ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
STORED AS RCFILE;
--
INSERT OVERWRITE TABLE logs2 
SELECT city_id, agent[0], agent[1], agent[2] FROM (SELECT city_id, uparser(user_agent) AS agent FROM logs where city_id IS NOT NULL AND user_agent IS NOT NULL) tmp;
DROP TABLE IF EXISTS logs;