-- set exec engine
set hive.execution.engine=mr;

--create tmp tables with counted values  per city
CREATE TEMPORARY TABLE devices AS
select city_id, device, COUNT(*) as records from logs2
group by city_id, device
order by records desc;
--
CREATE TEMPORARY TABLE browsers AS
select city_id, browser, COUNT(*) as records from logs2
group by city_id, browser
order by records desc;
--
CREATE TEMPORARY TABLE systems AS
select city_id, os, COUNT(*) as records from logs2
group by city_id, os
order by records desc;

--create tmp tables with max values per city
CREATE TEMPORARY TABLE devices2 AS
SELECT t.city_id 
     , t.device
  FROM ( SELECT city_id 
              , MAX(records) AS max_records
           FROM devices
         GROUP
             BY city_id ) AS m
INNER
  JOIN devices AS t
    ON t.city_id = m.city_id
   AND t.records = m.max_records;
--   
CREATE TEMPORARY TABLE browsers2 AS
SELECT t.city_id 
     , t.browser
  FROM ( SELECT city_id 
              , MAX(records) AS max_records
           FROM browsers
         GROUP
             BY city_id ) AS m
INNER
  JOIN browsers AS t
    ON t.city_id = m.city_id
   AND t.records = m.max_records;   
--
CREATE TEMPORARY TABLE systems2 AS
SELECT t.city_id 
     , t.os
  FROM ( SELECT city_id 
              , MAX(records) AS max_records
           FROM systems
         GROUP
             BY city_id ) AS m
INNER
  JOIN systems AS t
    ON t.city_id = m.city_id
   AND t.records = m.max_records;

--collect result
SELECT cities.name, devices2.device, browsers2.browser, systems2.os FROM devices2
JOIN cities ON cities.id = devices2.city_id 
JOIN browsers2 ON browsers2.city_id = devices2.city_id
JOIN systems2 ON systems2.city_id = devices2.city_id;