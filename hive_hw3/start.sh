#!/usr/bin/env bash
#set variables
inputDir=/input_data/artsiom_zeliankou/hive/task3
outputDir=/output_data/artsiom_zeliankou/hive/task3
inputfile=$1
citiesDir=${inputDir}/cities
logsDir=${inputDir}/biddings
HIVE_SCRIPT='hive.sh'
#
#prepare input directory
hdfs dfs -rm -r $inputDir
hdfs dfs -mkdir -p $inputDir
hdfs dfs -rm -r $outputDir
#
hdfs dfs -mkdir -p $citiesDir $logsDir
unzip -o $inputfile
hdfs dfs -copyFromLocal imp/city.* $citiesDir
hdfs dfs -copyFromLocal imp/imp.* $logsDir
rm -rf imp
#
mvn clean install
#
hdfs dfs -copyFromLocal target/task3-1.0-SNAPSHOT-jar-with-dependencies.jar ${inputDir}/
#
sh $HIVE_SCRIPT $citiesDir $logsDir