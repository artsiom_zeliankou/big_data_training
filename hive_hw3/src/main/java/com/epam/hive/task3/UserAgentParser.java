package com.epam.hive.task3;

        import eu.bitwalker.useragentutils.UserAgent;
        import org.apache.hadoop.hive.ql.exec.Description;
        import org.apache.hadoop.hive.ql.exec.UDF;
        import org.apache.hadoop.io.Text;

        import java.util.ArrayList;
        import java.util.List;

@Description(
        name = "uagent",
        value = "_FUNC_(str) - Return [Device, Browser, OS] from user agent"
)
public class UserAgentParser extends UDF {

    public List<Text> evaluate(final Text inputLine) {

        List<Text> result = new ArrayList<>();

        String userAgentString = inputLine.toString();
        if (userAgentString != null) {
            UserAgent userAgent = UserAgent.parseUserAgentString(userAgentString);
            result.add(new Text(userAgent.getOperatingSystem().getDeviceType().getName()));
            result.add(new Text(userAgent.getBrowser().getName()));
            result.add(new Text(userAgent.getOperatingSystem().getName()));
        }
        return result;
    }

}
