package task3;

import com.epam.hive.task3.UserAgentParser;
import eu.bitwalker.useragentutils.UserAgent;
import org.apache.hadoop.hive.ql.exec.UDF;
import org.apache.hadoop.io.Text;
import org.junit.Assert;
import org.junit.Test;
import java.util.List;


public class UserAgentParserTest extends UDF {

    @Test
    public void testUDFValidInput() {

        final String userAgentString = "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0)";
        final Text device = new Text("Computer");
        final Text browser = new Text("Internet Explorer 7");
        final Text os = new Text("Windows XP");

        List<Text> result = new UserAgentParser().evaluate(new Text(userAgentString));

        Assert.assertNotNull(result);
        Assert.assertFalse(result.isEmpty());
        Assert.assertTrue(device.equals(result.get(0)));
        Assert.assertTrue(browser.equals(result.get(1)));
        Assert.assertTrue(os.equals(result.get(2)));
    }

}
